    import axios from 'axios';
    const API_LAYER = `http://localhost:9200`;

    const apiLayer= axios.create({
        baseURL: API_LAYER,
    });

    export async function fetchTiendas() {
        try {
          const response = await apiLayer.get('/tiendas'); // Ajusta la ruta según tu API
          return response.data;
        } catch (error) {
          console.error('Error al obtener las tiendas:', error);
          return [];
        }
      }

    export default apiLayer;