import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import apiLayer from './apiLayer.js';

function CardGroupTiendas() {
  const [tiendas, setTiendas] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await apiLayer.get('/tiendas'); // Ajusta la ruta según tu API
        setTiendas(response.data);
        setLoading(false);
      } catch (error) {
        console.error('Error al obtener las tiendas:', error);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  return (
    <Container>
      {loading ? (
        <p>Cargando tiendas...</p>
      ) : (
        <Row>
          {tiendas.map((tienda) => (
            <Col key={tienda.ID_Tienda} xs={12} sm={6} md={4} lg={3}>
              <Card>
                <Card.Img variant="top" src={tienda.Imagen} alt={tienda.Nombre} />
                <Card.Body>
                  <Card.Title>{tienda.Nombre}</Card.Title>
                  <Card.Text>{tienda.Descripcion}</Card.Text>
                  <Card.Text>Calificación: {tienda.Calificacion}</Card.Text>
                  <Card.Text>Número de Contacto: {tienda.NumeroDeContacto}</Card.Text>
                  <Card.Text>Tipo: {tienda.Tipo}</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      )}
    </Container>
  );
}

export default CardGroupTiendas;
