import Badge from 'react-bootstrap/Badge';
import ListGroup from 'react-bootstrap/ListGroup';

function ListCategorias() {
  return (
    <ListGroup as="ol" numbered>
      <ListGroup.Item
        as="li"
        className="d-flex justify-content-between align-items-start"
      >
        <div className="ms-2 me-auto">
          <div className="fw-bold">Categoria 1</div>
          Descripción
        </div>
        <Badge bg="primary" pill>
          no tengo padre
        </Badge>
      </ListGroup.Item>
      <ListGroup.Item
        as="li"
        className="d-flex justify-content-between align-items-start"
      >
        <div className="ms-2 me-auto">
          <div className="fw-bold">Categoria 2</div>
          Descripción
        </div>
        <Badge bg="primary" pill>
          Padre
        </Badge>
      </ListGroup.Item>
      <ListGroup.Item
        as="li"
        className="d-flex justify-content-between align-items-start"
      >
        <div className="ms-2 me-auto">
          <div className="fw-bold">Categoria 3</div>
          Descripción
        </div>
        <Badge bg="primary" pill>
          Padre
        </Badge>
      </ListGroup.Item>
    </ListGroup>
  );
}

export default ListCategorias;