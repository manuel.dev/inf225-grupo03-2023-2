import React from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

function CardTienda(props) {
  const { nombre, descripcion, numeroDeContacto, imagenSrc } = props;
  const imagenStyle = {
    maxWidth: '100%', // Cambia el valor según el ancho máximo deseado
    height: 'auto',
  };

  return (
    <Card style={{ width: '12rem' }}>
      <Card.Body>
        <Card.Title>{nombre}</Card.Title>
        <Card.Text>{descripcion}</Card.Text>
        <Card.Text>{numeroDeContacto}</Card.Text>
        <Button variant="primary">Ver tienda</Button>
      </Card.Body>
    </Card>
  );
}

export default CardTienda;
