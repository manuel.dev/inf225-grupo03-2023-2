import React, { useState } from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

function HeaderTienda() {
  // Utilizamos el estado para controlar si el usuario está autenticado o no
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [showLoginModal, setShowLoginModal] = useState(false);

  // Función para manejar el inicio de sesión (cambiar isLoggedIn a true) y abrir el modal
  const handleLogin = () => {
    setIsLoggedIn(true);
    setShowLoginModal(true);
  };

  // Función para manejar el cierre de sesión (cambiar isLoggedIn a false)
  const handleLogout = () => {
    setIsLoggedIn(false);
  };

  // Función para abrir el modal de inicio de sesión
  const handleShowLoginModal = () => {
    setShowLoginModal(true);
  };

  // Función para cerrar el modal de inicio de sesión
  const handleCloseLoginModal = () => {
    setShowLoginModal(false);
  };

  return (
    <Navbar className="bg-body-tertiary">
      <Container>
        <Navbar.Brand href="#home">Tienda Colaborativa</Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          {isLoggedIn ? (
            <div>
              {/* Contenido cuando el usuario está autenticado */}
              <Button variant="primary" onClick={handleLogout}>
                Cerrar sesión
              </Button>
            </div>
          ) : (
            <div>
              {/* Botón de inicio de sesión */}
              <Button variant="primary" onClick={handleShowLoginModal}>
                Iniciar sesión
              </Button>
            </div>
          )}
        </Navbar.Collapse>
      </Container>
      {/* Modal de inicio de sesión */}
      <Modal show={showLoginModal} onHide={handleCloseLoginModal}>
        <Modal.Header closeButton>
          <Modal.Title>Iniciar sesión</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
              <Form.Label column sm={2}>
                Email
              </Form.Label>
              <Col sm={10}>
                <Form.Control type="email" placeholder="Email" />
              </Col>
            </Form.Group>

            <Form.Group as={Row} className="mb-3" controlId="formHorizontalPassword">
              <Form.Label column sm={2}>
                Password
              </Form.Label>
              <Col sm={10}>
                <Form.Control type="password" placeholder="Password" />
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-3" controlId="formHorizontalCheck">
              <Col sm={{ span: 10, offset: 2 }}>
                <Form.Check label="Remember me" />
              </Col>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseLoginModal}>
            Cerrar
          </Button>
          <Button variant="primary" onClick={handleLogin}>
            Iniciar sesión
          </Button>
        </Modal.Footer>
      </Modal>
    </Navbar>
  );
}

export default HeaderTienda;
