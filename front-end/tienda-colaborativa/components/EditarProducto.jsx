import React, { useState } from 'react';
import { Modal, Button, Form, Image } from 'react-bootstrap';

function EditarProducto() {
  const [show, setShow] = useState(false);
  const [productName, setProductName] = useState('Producto');
  const [productDescription, setProductDescription] = useState('Descripción del producto');
  const [productPrice, setProductPrice] = useState(0);
  const [productImage, setProductImage] = useState('URL de la imagen del producto');

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Editar Producto
      </Button>

      <Modal show={show} onHide={handleClose} size="lg">
        <Modal.Header closeButton>
          <Modal.Title>Editar Producto</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Image src={productImage} alt={productName} thumbnail />
          <Form>
            <Form.Group controlId="productName">
              <Form.Label>Nombre del Producto</Form.Label>
              <Form.Control
                type="text"
                value={productName}
                onChange={(e) => setProductName(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="productDescription">
              <Form.Label>Descripción</Form.Label>
              <Form.Control
                as="textarea"
                value={productDescription}
                onChange={(e) => setProductDescription(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="productPrice">
              <Form.Label>Precio</Form.Label>
              <Form.Control
                type="number"
                value={productPrice}
                onChange={(e) => setProductPrice(e.target.value)}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cerrar
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Guardar Cambios
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default EditarProducto;
