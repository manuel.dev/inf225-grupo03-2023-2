import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import CrearCategroria from '../components/crearCategoria.jsx';
import HeaderTienda from '../components/HeaderTienda';
import CardTienda from '../components/CardTienda';
import CardGroupTiendas from '../components/CardGroupTiendas';
import PaginacionTiendas from '../components/PaginacionTiendas';
import HeaderTiendaLogueado from '../components/HeaderTiendaLogueado';
import Login from '../components/Login';
import TiendasPage from '../pages/TiendasPage';
import CategoriaGestorColaborativoPage from '../pages/CategoriasGestorColaborativoPage';
import ProductosEmprendedorPage from '../pages/ProductosEmprendedorPage';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import Home from '../pages/Home.jsx';

function App() {
 
  return (
    <>
    <Router>
    <Routes>
        <Route path="/" element={<Home/>} >
        <Route path="/TiendasPage" element={<TiendasPage/>} />
        <Route path="/CategoriaGestorColaborativoPage" element={<CategoriaGestorColaborativoPage/>} />
        <Route path="/ProductosEmprendedorPage" element={<ProductosEmprendedorPage/>} />
      </Route>
    </Routes>
    </Router>
    </>
  )
}

export default App
