import { useState } from 'react'
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import CrearCategroria from '../components/crearCategoria.jsx';
import HeaderTienda from '../components/HeaderTienda';
import CardTienda from '../components/CardTienda';
import CardGroupTiendas from '../components/CardGroupTiendas';
import PaginacionTiendas from '../components/PaginacionTiendas';
import HeaderTiendaLogueado from '../components/HeaderTiendaLogueado';
import Login from '../components/Login';

function TiendasPage() {
  const [count, setCount] = useState(0)
  const tiendas = [
    { nombre: "Tienda 1", descripcion: "Descripción de la tienda 1", numeroDeContacto: "123456789", imagenSrc: "URL_de_la_imagen_1" },
    { nombre: "Tienda 2", descripcion: "Descripción de la tienda 2", numeroDeContacto: "987654321", imagenSrc: "URL_de_la_imagen_2" },
  ];

  const tiendasPerPage = 12; // Cantidad de tiendas por página
  const [currentPage, setCurrentPage] = useState(1);

  const onPageChange = (newPage) => {
    setCurrentPage(newPage); // Actualiza el estado de la página actual
  };

  const totalPages = Math.ceil(tiendas.length / tiendasPerPage);


  return (
    <>
    <header>
      <HeaderTienda/>
    </header>
    <body>
      <div className="App">
      <CardGroupTiendas
        tiendas={tiendas}
        currentPage={currentPage}
        tiendasPerPage={tiendasPerPage}
      />
      <div className="d-flex justify-content-center">
      <PaginacionTiendas
        currentPage={currentPage}
        totalPages={totalPages}
        onPageChange={onPageChange}
      />
      </div>  
      </div>
    </body>
    </>
  )
}

export default TiendasPage
