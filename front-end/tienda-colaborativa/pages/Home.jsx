import { Outlet, Link} from "react-router-dom";
import PaginacionTiendas from '../components/PaginacionTiendas';
import HeaderTiendaLogueado from '../components/HeaderTiendaLogueado';
import Login from '../components/Login';
import TiendasPage from '../pages/TiendasPage';
import CategoriaGestorColaborativoPage from '../pages/CategoriasGestorColaborativoPage';
import ProductosEmprendedorPage from '../pages/ProductosEmprendedorPage';

const Home = () =>{
    return <div>
        <nav>
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/TiendasPage">Tiendas</Link>
                </li>
                <li>
                    <Link to="/CategoriaGestorColaborativoPage">Gestor</Link>
                </li>
                <li>
                    <Link to="/ProductosEmprendedorPage">Emprendedor</Link>
                </li>
            </ul>
        </nav>
        <Outlet/>

    </div>
}

export default Home;