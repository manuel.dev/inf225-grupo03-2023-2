import { useState } from 'react'
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import CrearCategroria from '../components/crearCategoria.jsx';
import HeaderTiendaLogueado from '../components/HeaderTiendaLogueado.jsx';
import ListCategorias from '../components/ListCategorias.jsx';

function CategoriaGestorColaborativoPage() {
  const [count, setCount] = useState(0)

  return (
    <>
    <HeaderTiendaLogueado/>
    <CrearCategroria/>
    <ListCategorias/>
    </>
  )
}

export default CategoriaGestorColaborativoPage