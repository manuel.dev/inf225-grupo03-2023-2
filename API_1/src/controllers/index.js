const database = require('../db');

//Esta tabla es solo un ejemplo para que puedan ver como se hacen querys a la bdd, después puede ser eliminada
const createTable = (req, res) => {
    const query =  'CREATE TABLE IF NOT EXISTS user('+
        '  id int AUTO_INCREMENT, '+
        'firstname VARCHAR(50), '+
        'lastname VARCHAR(50), '+
        'email VARCHAR(50), '+
        'PRIMARY KEY(id))';

    database.query(query, (err) => {
        if (err) throw err;
        res.send('Tabla creada')
    });
};

const createTableDeProductos = (req,res) => {
    const createTableQuery = `
        CREATE TABLE IF NOT EXISTS productos (
            ID INT AUTO_INCREMENT PRIMARY KEY,
            ID_Tienda INT,
            Nombre VARCHAR(255),
            Precio INT,
            Calificacion INT
        )
    `;
    
    database.query(createTableQuery, (err) => {
        if (err) {
            throw err;
        } else {
            res.send('Tabla de productos creada');
        }
    });
};

const createProduct = (req, res) => {
    const { ID_Tienda, Nombre, Precio, Calificacion } = req.body;
    const product = {
        ID_Tienda,
        Nombre,
        Precio,
        Calificacion,
    };
    const query = 'INSERT INTO productos SET ?';
    database.query(query, product, (err, result) => {
        if (err) {
            console.error('Error al crear el producto:', err);
            res.status(500).send('Error al crear el producto');
            return;
        }
        res.send('Producto creado con ID: ' + result.insertId);
    });
}

const getAllProducts = (req, res) => {
    const query = 'SELECT * FROM productos';

    database.query(query, (err, rows) => {
        if (err) {
            console.error('Error al obtener los productos:', err);
            res.status(500).send('Error al obtener los productos');
            return;
        }

        res.send(rows);
    });
};

const getProductById = (req, res) => {
    const productId = req.params.id; // Obtener el ID del parámetro de la URL

    const query = 'SELECT * FROM productos WHERE ID = ?';

    database.query(query, [productId], (err, rows) => {
        if (err) {
            console.error('Error al obtener el producto:', err);
            res.status(500).send('Error al obtener el producto');
            return;
        }

        if (rows.length === 0) {
            res.status(404).send('Producto no encontrado'); // Enviar una respuesta 404 si no se encuentra el producto
        } else {
            res.send(rows[0]); // Enviar el producto encontrado
        }
    });
};

const updateProduct = (req, res) => {
    const productId = req.params.id; // Obtener el ID del producto a actualizar desde los parámetros de la URL
    const { ID_Tienda, Nombre, Precio, Calificacion } = req.body; // Obtener los nuevos datos del producto desde el cuerpo de la solicitud

    const updatedProduct = {
        ID_Tienda,
        Nombre,
        Precio,
        Calificacion,
    };

    const query = 'UPDATE productos SET ? WHERE ID = ?';

    database.query(query, [updatedProduct, productId], (err, result) => {
        if (err) {
            console.error('Error al actualizar el producto:', err);
            res.status(500).send('Error al actualizar el producto');
            return;
        }

        if (result.affectedRows === 0) {
            res.status(404).send('Producto no encontrado'); // Enviar una respuesta 404 si no se encuentra el producto
        } else {
            res.send('Producto actualizado con éxito');
        }
    });
};

const deleteProduct = (req, res) => {
    const productId = req.params.id; // Obtener el ID del producto a eliminar desde los parámetros de la URL

    const query = 'DELETE FROM productos WHERE ID = ?';

    database.query(query, [productId], (err, result) => {
        if (err) {
            console.error('Error al eliminar el producto:', err);
            res.status(500).send('Error al eliminar el producto');
            return;
        }

        if (result.affectedRows === 0) {
            res.status(404).send('Producto no encontrado'); // Enviar una respuesta 404 si no se encuentra el producto
        } else {
            res.send('Producto eliminado con éxito');
        }
    });
};

//CRUD DE CATEGORIAS

const createCategoryTable = (req, res) => {
    const createTableQuery = `
        CREATE TABLE IF NOT EXISTS categorias (
            ID INT AUTO_INCREMENT PRIMARY KEY,
            Nombre VARCHAR(255) NOT NULL,
            Descripcion TEXT,
            CategoriaPadreID INT,
            FOREIGN KEY (CategoriaPadreID) REFERENCES categorias(ID)
        )
    `;

    database.query(createTableQuery, (err) => {
        if (err) {
            throw err;
        } else {
            res.send('Tabla de categorías creada');
        }
    });
};

// Crear una nueva categoría
const createCategory = (req, res) => {
    const { Nombre, Descripcion, CategoriaPadreID } = req.body;
    const category = {
        Nombre,
        Descripcion,
        CategoriaPadreID,
    };
    
    const query = 'INSERT INTO categorias SET ?';

    database.query(query, category, (err, result) => {
        if (err) {
            console.error('Error al crear la categoría:', err);
            res.status(500).send('Error al crear la categoría');
            return;
        }
        
        res.send('Categoría creada con ID: ' + result.insertId);
    });
};

// Obtener todas las categorías
const getAllCategories = (req, res) => {
    const query = 'SELECT * FROM categorías';

    database.query(query, (err, rows) => {
        if (err) {
            console.error('Error al obtener las categorías:', err);
            res.status(500).send('Error al obtener las categorías');
            return;
        }
        
        res.send(rows);
    });
};

// Obtener una categoría por ID
const getCategoryById = (req, res) => {
    const categoryId = req.params.id;

    const query = 'SELECT * FROM categorias WHERE ID = ?';

    database.query(query, [categoryId], (err, rows) => {
        if (err) {
            console.error('Error al obtener la categoría:', err);
            res.status(500).send('Error al obtener la categoría');
            return;
        }
        
        if (rows.length === 0) {
            res.status(404).send('Categoría no encontrada');
        } else {
            res.send(rows[0]);
        }
    });
};

// Actualizar una categoría por ID
const updateCategory = (req, res) => {
    const categoryId = req.params.id;
    const { Nombre, Descripcion, CategoriaPadreID } = req.body;
    const updatedCategory = {
        Nombre,
        Descripcion,
        CategoriaPadreID,
    };
    
    const query = 'UPDATE categorias SET ? WHERE ID = ?';

    database.query(query, [updatedCategory, categoryId], (err, result) => {
        if (err) {
            console.error('Error al actualizar la categoría:', err);
            res.status(500).send('Error al actualizar la categoría');
            return;
        }
        
        if (result.affectedRows === 0) {
            res.status(404).send('Categoría no encontrada');
        } else {
            res.send('Categoría actualizada con éxito');
        }
    });
};

// Eliminar una categoría por ID
const deleteCategory = (req, res) => {
    const categoryId = req.params.id;

    const query = 'DELETE FROM categorias WHERE ID = ?';

    database.query(query, [categoryId], (err, result) => {
        if (err) {
            console.error('Error al eliminar la categoría:', err);
            res.status(500).send('Error al eliminar la categoría');
            return;
        }
        
        if (result.affectedRows === 0) {
            res.status(404).send('Categoría no encontrada');
        } else {
            res.send('Categoría eliminada con éxito');
        }
    });
};

//ProductoCategoria

const createProductCategoryTable = (req, res) => {
    const createTableQuery = `
        CREATE TABLE IF NOT EXISTS producto_categoria (
            ProductoID INT,
            CategoriaID INT,
            PRIMARY KEY (ProductoID, CategoriaID),
            FOREIGN KEY (ProductoID) REFERENCES productos(ID),
            FOREIGN KEY (CategoriaID) REFERENCES categorias(ID)
        )
    `;

    database.query(createTableQuery, (err) => {
        if (err) {
            throw err;
        } else {
            res.send('Tabla de relación producto-categoría creada');
        }
    });
};

// Asignar una categoría a un producto
const assignCategoryToProduct = (req, res) => {
    const { ProductoID, CategoriaID } = req.body;
    const productCategory = {
        ProductoID,
        CategoriaID,
    };

    const query = 'INSERT INTO producto_categoria SET ?';

    database.query(query, productCategory, (err, result) => {
        if (err) {
            console.error('Error al asignar la categoría al producto:', err);
            res.status(500).send('Error al asignar la categoría al producto');
            return;
        }

        res.send('Categoría asignada al producto con éxito');
    });
};

// Obtener todas las categorías asignadas a un producto por ID de producto
const getCategoriesByProductID = (req, res) => {
    const productId = req.params.id;

    const query = 'SELECT CategoriaID FROM producto_categoria WHERE ProductoID = ?';

    database.query(query, [productId], (err, rows) => {
        if (err) {
            console.error('Error al obtener las categorías del producto:', err);
            res.status(500).send('Error al obtener las categorías del producto');
            return;
        }

        res.send(rows);
    });
};

// Obtener todos los productos asociados a una categoría por ID de categoría
const getProductsByCategoryID = (req, res) => {
    const categoryId = req.params.id;

    const query = 'SELECT p.* FROM productos p JOIN producto_categoria pc ON p.ID = pc.ProductoID WHERE pc.CategoriaID = ?';

    database.query(query, [categoryId], (err, rows) => {
        if (err) {
            console.error('Error al obtener los productos asociados a la categoría:', err);
            res.status(500).send('Error al obtener los productos asociados a la categoría');
            return;
        }

        res.send(rows);
    });
};


// Eliminar una categoría asignada a un producto por ID de producto y ID de categoría
const removeCategoryFromProduct = (req, res) => {
    const { ProductoID, CategoriaID } = req.body;

    const query = 'DELETE FROM producto_categoria WHERE ProductoID = ? AND CategoriaID = ?';

    database.query(query, [ProductoID, CategoriaID], (err, result) => {
        if (err) {
            console.error('Error al eliminar la asignación de categoría al producto:', err);
            res.status(500).send('Error al eliminar la asignación de categoría al producto');
            return;
        }

        if (result.affectedRows === 0) {
            res.status(404).send('Asignación de categoría no encontrada');
        } else {
            res.send('Asignación de categoría eliminada con éxito');
        }
    });
};


module.exports = {
    createTable,
    createTableDeProductos,
    createProduct,
    getAllProducts,
    getProductById,
    updateProduct,
    deleteProduct,

    //categorias
    createCategoryTable,
    createCategory,
    getAllCategories,
    getCategoryById,
    updateCategory,
    deleteCategory,

    //categoria producto
    createProductCategoryTable,
    assignCategoryToProduct,
    getCategoriesByProductID,
    getProductsByCategoryID,
    removeCategoryFromProduct
}