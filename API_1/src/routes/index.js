const express = require('express');
const controller = require('../controllers/index');
const router = new express.Router();

router.get('/createTable', controller.createTable);

//endpoint para productos

router.post('/createTableDeProductos', controller.createTableDeProductos)

router.post('/productos', controller.createProduct)

router.get('/productos', controller.getAllProducts)

router.get('/productos/:id', controller.getProductById)

router.put('/productos/:id', controller.updateProduct)

router.delete('/productos/:id', controller.deleteProduct)

//endpoint para categorias

router.get('/createCategoryTable', controller.createCategoryTable);
router.post('/categorias', controller.createCategory);
router.get('/categorias', controller.getAllCategories);
router.get('/categorias/:id', controller.getCategoryById);
router.put('/categorias/:id', controller.updateCategory);
router.delete('/categorias/:id', controller.deleteCategory);

// Endpoints para la relación producto_categoria
router.get('/productos/:id_producto/categorias', controller.getCategoriesByProductID);
router.post('/productos/:id_producto/categorias', controller.assignCategoryToProduct);
router.delete('/productos/:id_producto/categorias/:id_categoria', controller.removeCategoryFromProduct);
router.get('/categorias/:id_categoria/productos', controller.getProductsByCategoryID);



module.exports = router;