# INF225-GRUPO03-2023-2

**Nombre y rol de los integrantes:**

* Manuel De La Hoz 202004064-8

* Joaquín Fernández 201973124-6

* Patricio Fernández 201973010-k

* Catalina González 202130010-4


**Enlace al video con la presentación del cliente:** https://www.youtube.com/watch?v=NI3GSDZrnHY&ab_channel=WladimirOrmaz%C3%A1bal

**Enlace al video de la primera entrega del grupo:** https://www.youtube.com/watch?v=rDaWR3TT4m4&feature=youtu.be&ab_channel=ManuelDe

**Un enlace a la Wiki con la cual se va a trabajar:** https://gitlab.com/manuel.dev/inf225-grupo03-2023-2/-/wikis/home

**Presentación Hito 04 (Prototipo 1):** https://youtu.be/k6vfHYFkx48

**Demostración Hito 07:** https://youtu.be/sxUC00ZJBDM
