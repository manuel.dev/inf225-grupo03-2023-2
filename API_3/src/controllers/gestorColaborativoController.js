const database = require('../db');

const createGestorColaborativoTable = (req, res) => {
    const createTableQuery = `
        CREATE TABLE IF NOT EXISTS gestores_colaborativos (
            ID INT AUTO_INCREMENT PRIMARY KEY,
            rut VARCHAR(255) NOT NULL,
            nombre VARCHAR(255) NOT NULL,
            correo VARCHAR(255) NOT NULL,
            contraseña VARCHAR(255) NOT NULL
        )
    `;

    database.query(createTableQuery, (err) => {
        if (err) {
            throw err;
        } else {
            res.send('Tabla de Gestores Colaborativos creada');
        }
    });
};

const createGestorColaborativo = (req, res) => {
    const { rut, nombre, correo, contraseña } = req.body;
    const gestorColaborativo = {
        rut,
        nombre,
        correo,
        contraseña,
    };
    
    const query = 'INSERT INTO gestores_colaborativos SET ?';

    database.query(query, gestorColaborativo, (err, result) => {
        if (err) {
            console.error('Error al crear el Gestor Colaborativo:', err);
            res.status(500).send('Error al crear el Gestor Colaborativo');
            return;
        }
        
        res.send('Gestor Colaborativo creado con ID: ' + result.insertId);
    });
};

const getAllGestoresColaborativos = (req, res) => {
    const query = 'SELECT * FROM gestores_colaborativos';

    database.query(query, (err, rows) => {
        if (err) {
            console.error('Error al obtener los Gestores Colaborativos:', err);
            res.status(500).send('Error al obtener los Gestores Colaborativos');
            return;
        }

        res.send(rows);
    });
};

const getGestorColaborativoById = (req, res) => {
    const gestorColaborativoId = req.params.id;

    const query = 'SELECT * FROM gestores_colaborativos WHERE ID = ?';

    database.query(query, [gestorColaborativoId], (err, rows) => {
        if (err) {
            console.error('Error al obtener el Gestor Colaborativo:', err);
            res.status(500).send('Error al obtener el Gestor Colaborativo');
            return;
        }
        
        if (rows.length === 0) {
            res.status(404).send('Gestor Colaborativo no encontrado');
        } else {
            res.send(rows[0]);
        }
    });
};

const updateGestorColaborativo = (req, res) => {
    const gestorColaborativoId = req.params.id;
    const { rut, nombre, correo, contraseña } = req.body;
    const updatedGestorColaborativo = {
        rut,
        nombre,
        correo,
        contraseña,
    };
    
    const query = 'UPDATE gestores_colaborativos SET ? WHERE ID = ?';

    database.query(query, [updatedGestorColaborativo, gestorColaborativoId], (err, result) => {
        if (err) {
            console.error('Error al actualizar el Gestor Colaborativo:', err);
            res.status(500).send('Error al actualizar el Gestor Colaborativo');
            return;
        }
        
        if (result.affectedRows === 0) {
            res.status(404).send('Gestor Colaborativo no encontrado');
        } else {
            res.send('Gestor Colaborativo actualizado con éxito');
        }
    });
};

const deleteGestorColaborativo = (req, res) => {
    const gestorColaborativoId = req.params.id;

    const query = 'DELETE FROM gestores_colaborativos WHERE ID = ?';

    database.query(query, [gestorColaborativoId], (err, result) => {
        if (err) {
            console.error('Error al eliminar el Gestor Colaborativo:', err);
            res.status(500).send('Error al eliminar el Gestor Colaborativo');
            return;
        }
        
        if (result.affectedRows === 0) {
            res.status(404).send('Gestor Colaborativo no encontrado');
        } else {
            res.send('Gestor Colaborativo eliminado con éxito');
        }
    });
};

module.exports = {
    createGestorColaborativoTable,
    createGestorColaborativo,
    getAllGestoresColaborativos,
    getGestorColaborativoById,
    updateGestorColaborativo,
    deleteGestorColaborativo
}