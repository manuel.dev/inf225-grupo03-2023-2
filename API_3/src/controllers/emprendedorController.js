const database = require('../db');

const createEmprendedorTable = (req, res) => {
    const createTableQuery = `
        CREATE TABLE IF NOT EXISTS emprendedores (
            ID INT AUTO_INCREMENT PRIMARY KEY,
            rut VARCHAR(255) NOT NULL,
            nombre VARCHAR(255) NOT NULL,
            correo VARCHAR(255) NOT NULL,
            contraseña VARCHAR(255) NOT NULL
        )
    `;

    database.query(createTableQuery, (err) => {
        if (err) {
            throw err;
        } else {
            res.send('Tabla de Emprendedores creada');
        }
    });
};

const createEmprendedor = (req, res) => {
    const { rut, nombre, correo, contraseña } = req.body;
    const emprendedor = {
        rut,
        nombre,
        correo,
        contraseña,
    };
    
    const query = 'INSERT INTO emprendedores SET ?';

    database.query(query, emprendedor, (err, result) => {
        if (err) {
            console.error('Error al crear el Emprendedor:', err);
            res.status(500).send('Error al crear el Emprendedor');
            return;
        }
        
        res.send('Emprendedor creado con ID: ' + result.insertId);
    });
};

const getAllEmprendedores = (req, res) => {
    const query = 'SELECT * FROM emprendedores';

    database.query(query, (err, rows) => {
        if (err) {
            console.error('Error al obtener los Emprendedores:', err);
            res.status(500).send('Error al obtener los Emprendedores');
            return;
        }

        res.send(rows);
    });
};

const getEmprendedorById = (req, res) => {
    const emprendedorId = req.params.id;

    const query = 'SELECT * FROM emprendedores WHERE ID = ?';

    database.query(query, [emprendedorId], (err, rows) => {
        if (err) {
            console.error('Error al obtener el Emprendedor:', err);
            res.status(500).send('Error al obtener el Emprendedor');
            return;
        }
        
        if (rows.length === 0) {
            res.status(404).send('Emprendedor no encontrado');
        } else {
            res.send(rows[0]);
        }
    });
};

const updateEmprendedor = (req, res) => {
    const emprendedorId = req.params.id;
    const { rut, nombre, correo, contraseña } = req.body;
    const updatedEmprendedor = {
        rut,
        nombre,
        correo,
        contraseña,
    };
    
    const query = 'UPDATE emprendedores SET ? WHERE ID = ?';

    database.query(query, [updatedEmprendedor, emprendedorId], (err, result) => {
        if (err) {
            console.error('Error al actualizar el Emprendedor:', err);
            res.status(500).send('Error al actualizar el Emprendedor');
            return;
        }
        
        if (result.affectedRows === 0) {
            res.status(404).send('Emprendedor no encontrado');
        } else {
            res.send('Emprendedor actualizado con éxito');
        }
    });
};

const deleteEmprendedor = (req, res) => {
    const emprendedorId = req.params.id;

    const query = 'DELETE FROM emprendedores WHERE ID = ?';

    database.query(query, [emprendedorId], (err, result) => {
        if (err) {
            console.error('Error al eliminar el Emprendedor:', err);
            res.status(500).send('Error al eliminar el Emprendedor');
            return;
        }
        
        if (result.affectedRows === 0) {
            res.status(404).send('Emprendedor no encontrado');
        } else {
            res.send('Emprendedor eliminado con éxito');
        }
    });
};

module.exports = {
    createEmprendedorTable,
    createEmprendedor,
    getAllEmprendedores,
    getEmprendedorById,
    updateEmprendedor,
    deleteEmprendedor,
};