const database = require('../db');

const createClienteTable = (req, res) => {
    const createTableQuery = `
        CREATE TABLE IF NOT EXISTS clientes (
            ID INT AUTO_INCREMENT PRIMARY KEY,
            rut VARCHAR(255) NOT NULL,
            nombre VARCHAR(255) NOT NULL,
            correo VARCHAR(255) NOT NULL,
            contraseña VARCHAR(255) NOT NULL
        )
    `;

    database.query(createTableQuery, (err) => {
        if (err) {
            throw err;
        } else {
            res.send('Tabla de Clientes creada');
        }
    });
};

const createCliente = (req, res) => {
    const { rut, nombre, correo, contraseña } = req.body;
    const cliente = {
        rut,
        nombre,
        correo,
        contraseña,
    };
    
    const query = 'INSERT INTO clientes SET ?';

    database.query(query, cliente, (err, result) => {
        if (err) {
            console.error('Error al crear el Cliente:', err);
            res.status(500).send('Error al crear el Cliente');
            return;
        }
        
        res.send('Cliente creado con ID: ' + result.insertId);
    });
};

const getAllClientes = (req, res) => {
    const query = 'SELECT * FROM clientes';

    database.query(query, (err, rows) => {
        if (err) {
            console.error('Error al obtener los Clientes:', err);
            res.status(500).send('Error al obtener los Clientes');
            return;
        }

        res.send(rows);
    });
};

const getClienteById = (req, res) => {
    const clienteId = req.params.id;

    const query = 'SELECT * FROM clientes WHERE ID = ?';

    database.query(query, [clienteId], (err, rows) => {
        if (err) {
            console.error('Error al obtener el Cliente:', err);
            res.status(500).send('Error al obtener el Cliente');
            return;
        }
        
        if (rows.length === 0) {
            res.status(404).send('Cliente no encontrado');
        } else {
            res.send(rows[0]);
        }
    });
};

const updateCliente = (req, res) => {
    const clienteId = req.params.id;
    const { rut, nombre, correo, contraseña } = req.body;
    const updatedCliente = {
        rut,
        nombre,
        correo,
        contraseña,
    };
    
    const query = 'UPDATE clientes SET ? WHERE ID = ?';

    database.query(query, [updatedCliente, clienteId], (err, result) => {
        if (err) {
            console.error('Error al actualizar el Cliente:', err);
            res.status(500).send('Error al actualizar el Cliente');
            return;
        }
        
        if (result.affectedRows === 0) {
            res.status(404).send('Cliente no encontrado');
        } else {
            res.send('Cliente actualizado con éxito');
        }
    });
};

const deleteCliente = (req, res) => {
    const clienteId = req.params.id;

    const query = 'DELETE FROM clientes WHERE ID = ?';

    database.query(query, [clienteId], (err, result) => {
        if (err) {
            console.error('Error al eliminar el Cliente:', err);
            res.status(500).send('Error al eliminar el Cliente');
            return;
        }
        
        if (result.affectedRows === 0) {
            res.status(404).send('Cliente no encontrado');
        } else {
            res.send('Cliente eliminado con éxito');
        }
    });
};

module.exports = {
    createClienteTable,
    createCliente,
    getAllClientes,
    getClienteById,
    updateCliente,
    deleteCliente
}