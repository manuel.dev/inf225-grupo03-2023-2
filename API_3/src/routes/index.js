const express = require('express');
const controller = require('../controllers/index');
const emprendedorController = require('../controllers/emprendedorController');
const clienteController = require('../controllers/clienteController');
const gestorColaborativoController = require('../controllers/gestorColaborativoController');
const router = new express.Router();

router.get('/createTable', controller.createTable);

// Endpoints para Emprendedores
router.post('/createEmprendedores', emprendedorController.createEmprendedorTable);
router.get('/emprendedores', emprendedorController.getAllEmprendedores);
router.get('/emprendedores/:id', emprendedorController.getEmprendedorById);
router.post('/emprendedores', emprendedorController.createEmprendedor);
router.put('/emprendedores/:id', emprendedorController.updateEmprendedor);
router.delete('/emprendedores/:id', emprendedorController.deleteEmprendedor);

// Endpoints para Clientes
router.post('/createClientes', clienteController.createClienteTable);
router.get('/clientes', clienteController.getAllClientes);
router.get('/clientes/:id', clienteController.getClienteById);
router.post('/clientes', clienteController.createCliente);
router.put('/clientes/:id', clienteController.updateCliente);
router.delete('/clientes/:id', clienteController.deleteCliente);

// Endpoints para Gestores Colaborativos
router.post('/createGestoresColaborativos', gestorColaborativoController.createGestorColaborativoTable);
router.get('/gestoresColaborativos', gestorColaborativoController.getAllGestoresColaborativos);
router.get('/gestoresColaborativos/:id', gestorColaborativoController.getGestorColaborativoById);
router.post('/gestoresColaborativos', gestorColaborativoController.createGestorColaborativo);
router.put('/gestoresColaborativos/:id', gestorColaborativoController.updateGestorColaborativo);
router.delete('/gestoresColaborativos/:id', gestorColaborativoController.deleteGestorColaborativo);


module.exports = router;