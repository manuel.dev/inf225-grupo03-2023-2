const {tiendasAPI} = require('../apis');
const API_URL_TIENDAS = `http://localhost:${process.env.PORT_API_TIENDAS}`;


const getAllTiendas = async (req, res) => {
    try {
      const response = await tiendasAPI.get('/tiendas'); // Usamos la instancia de tiendasAPI para hacer la solicitud
      res.json(response.data);
    } catch (error) {
      console.error('Error al obtener las tiendas:', error);
      res.status(500).send('Error al obtener las tiendas la envie a esta url: usando variables de entorno: '  + API_URL_TIENDAS+ '/tiendas');
    }
  };

  const createTienda = async (req, res) => {
    try {
        const { Nombre, Calificacion, NumeroDeContacto, Descripcion, Tipo } = req.body;
        const tienda = {
            Nombre,
            Calificacion,
            NumeroDeContacto,
            Descripcion,
            Tipo
        };
        const response = await tiendasAPI.post('/tiendas', tienda);
        res.json(response.data);
    } catch (error) {
        console.error('Error al crear la tienda:', error);
        res.status(500).send('Error al crear la tienda');
    }
};

const updateTienda = async (req, res) => {
    try {
        const tiendaId = req.params.id;
        const { Nombre, Calificacion, NumeroDeContacto, Descripcion, Tipo } = req.body;
        const tienda = {
            Nombre,
            Calificacion,
            NumeroDeContacto,
            Descripcion,
            Tipo
        };
        const response = await tiendasAPI.put(`/tiendas/${tiendaId}`, tienda);
        res.json(response.data);
    } catch (error) {
        console.error('Error al actualizar la tienda:', error);
        res.status(500).send('Error al actualizar la tienda');
    }
};

const deleteTienda = async (req, res) => {
    try {
        const tiendaId = req.params.id;
        const response = await tiendasAPI.delete(`/tiendas/${tiendaId}`);
        res.json(response.data);
    } catch (error) {
        console.error('Error al eliminar la tienda:', error);
        res.status(500).send('Error al eliminar la tienda');
    }
};

const getTiendaById = async (req, res) => {
    try {
        const tiendaId = req.params.id;
        const response = await tiendasAPI.get(`/tiendas/${tiendaId}`);
        res.json(response.data);
    } catch (error) {
        console.error('Error al obtener la tienda:', error);
        if (error.response && error.response.status === 404) {
            res.status(404).send('Tienda no encontrada');
        } else {
            res.status(500).send('Error al obtener la tienda');
        }
    }
};


  module.exports = {
    getAllTiendas,
    createTienda,
    updateTienda,
    deleteTienda,
    getTiendaById
}

