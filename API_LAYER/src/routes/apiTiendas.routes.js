const express = require('express');
const controllerTiendas = require('../controllers/apiTiendas.controllers');
const routerTiendas = new express.Router();

routerTiendas.get('/tiendas', controllerTiendas.getAllTiendas);
routerTiendas.get('/tiendas/:id', controllerTiendas.getTiendaById);
routerTiendas.post('/tiendas', controllerTiendas.createTienda);
routerTiendas.put('/tiendas/:id', controllerTiendas.updateTienda);
routerTiendas.delete('/tiendas/:id', controllerTiendas.deleteTienda);

module.exports = routerTiendas;