const axios = require('axios');
const dotenv = require('dotenv');

dotenv.config();

// Configuración de las URLs de las APIs
const API_URL_PRODUCTOS = `http://localhost:${process.env.PORT_API_PRODUCTOS}`;
const API_URL_TIENDAS = `http://localhost:${process.env.PORT_API_TIENDAS}`;
const API_URL_USUARIOS = `http://localhost:${process.env.PORT_API_USUARIOS}`;

// Crea instancias de Axios para cada API
const productosAPI = axios.create({
  baseURL: API_URL_PRODUCTOS,
});

const tiendasAPI = axios.create({
  baseURL: API_URL_TIENDAS,
});

const usuariosAPI = axios.create({
  baseURL: API_URL_USUARIOS,
});

module.exports = {
  productosAPI,
  tiendasAPI,
  usuariosAPI,
};
