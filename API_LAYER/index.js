const express = require('express');
const app = express();
require('dotenv').config();
const routerTiendas = require('./src/routes/apiTiendas.routes');

app.use(express.json());
app.use(routerTiendas);

app.listen(process.env.PORT_API, () => {
    console.log('Server running!');
});