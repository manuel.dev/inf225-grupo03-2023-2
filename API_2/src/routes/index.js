const express = require('express');
const controller = require('../controllers/index');
const router = new express.Router();

router.get('/createTable', controller.createTable);

router.get('/createTableDeTiendas', controller.createTableDeTiendas);

router.get('/tiendas', controller.getAllTiendas);
router.get('/tiendas/:id', controller.getTiendaById);
router.post('/tiendas', controller.createTienda);
router.put('/tiendas/:id', controller.updateTienda);
router.delete('/tiendas/:id', controller.deleteTienda);


module.exports = router;