const request = require('supertest');
const app = require('../../index');
const database = require('../../src/db');
const sinon = require('sinon');


describe('Prueba de obtención de Tiendas usando Get', () => {
  // Test para la ruta GET /tiendas
  it('Muestra todas las tiendas', async () => {
    const response = await request(app).get('/tiendas');
    expect(response.status).toBe(200);
    expect(Array.isArray(response.body)).toBe(true);
    console.log(response.body[0]);
  });
});
