const database = require('../db');

//Esta tabla es solo un ejemplo para que puedan ver como se hacen querys a la bdd, después puede ser eliminada
const createTable = (req, res) => {
    const query =  'CREATE TABLE IF NOT EXISTS user('+
        '  id int AUTO_INCREMENT, '+
        'firstname VARCHAR(50), '+
        'lastname VARCHAR(50), '+
        'email VARCHAR(50), '+
        'PRIMARY KEY(id))';

    database.query(query, (err) => {
        if (err) throw err;
        res.send('Tabla creada')
    });
};

const createTableDeTiendas = (req, res) => {
    const createTableQuery = `
        CREATE TABLE IF NOT EXISTS tiendas (
            ID_Tienda INT AUTO_INCREMENT PRIMARY KEY,
            Nombre VARCHAR(255),
            Calificacion INT,
            NumeroDeContacto INT,
            Descripcion VARCHAR(255),
            Tipo VARCHAR(255)
        )
    `;

    database.query(createTableQuery, (err) => {
        if (err) {
            throw err;
        } else {
            res.send('Tabla de tiendas creada');
        }
    });
};

const createTienda = (req, res) => {
    const { Nombre, Calificacion, NumeroDeContacto, Descripcion, Tipo } = req.body;
    const tienda = {
        Nombre,
        Calificacion,
        NumeroDeContacto,
        Descripcion,
        Tipo
    };
    const query = 'INSERT INTO tiendas SET ?';
    database.query(query, tienda, (err, result) => {
        if (err) {
            console.error('Error al crear la tienda:', err);
            res.status(500).send('Error al crear la tienda');
            return;
        }
        res.send('Tienda creada con ID: ' + result.insertId);
    });
};

const getAllTiendas = (req, res) => {
    const query = 'SELECT * FROM tiendas';

    database.query(query, (err, rows) => {
        if (err) {
            console.error('Error al obtener las tiendas:', err);
            res.status(500).send('Error al obtener las tiendas');
            return;
        }

        res.send(rows);
    });
};

const getTiendaById = (req, res) => {
    const tiendaId = req.params.id;
    const query = 'SELECT * FROM tiendas WHERE ID_Tienda = ?';

    database.query(query, [tiendaId], (err, rows) => {
        if (err) {
            console.error('Error al obtener la tienda:', err);
            res.status(500).send('Error al obtener la tienda');
            return;
        }

        if (rows.length === 0) {
            res.status(404).send('Tienda no encontrada');
        } else {
            res.send(rows[0]);
        }
    });
};

const updateTienda = (req, res) => {
    const tiendaId = req.params.id;
    const { Nombre, Calificacion, NumeroDeContacto, Descripcion, Tipo } = req.body;
    const tienda = {
        Nombre,
        Calificacion,
        NumeroDeContacto,
        Descripcion,
        Tipo
    };
    const query = 'UPDATE tiendas SET ? WHERE ID_Tienda = ?';

    database.query(query, [tienda, tiendaId], (err, result) => {
        if (err) {
            console.error('Error al actualizar la tienda:', err);
            res.status(500).send('Error al actualizar la tienda');
            return;
        }

        if (result.affectedRows === 0) {
            res.status(404).send('Tienda no encontrada');
        } else {
            res.send('Tienda actualizada');
        }
    });
};

const deleteTienda = (req, res) => {
    const tiendaId = req.params.id;
    const query = 'DELETE FROM tiendas WHERE ID_Tienda = ?';

    database.query(query, [tiendaId], (err, result) => {
        if (err) {
            console.error('Error al eliminar la tienda:', err);
            res.status(500).send('Error al eliminar la tienda');
            return;
        }

        if (result.affectedRows === 0) {
            res.status(404).send('Tienda no encontrada');
        } else {
            res.send('Tienda eliminada');
        }
    });
};


module.exports = {
    createTable,
    createTableDeTiendas,
    createTienda,
    getAllTiendas,
    getTiendaById,
    updateTienda,
    deleteTienda
}